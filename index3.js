const http = require('http');
const {v4:uuid4} = require('uuid');

const server = http.createServer((request,response)=>{
    if(request.method === 'GET'&& request.url ==='/uuid'){
        response.writeHead(200,{'content-type':'application/json'})
        const uuid = uuid4();
        response.end(JSON.stringify(uuid));
    }
    else{
        response.writeHead(400);
        response.end('404 not found');
    }
})

server.listen(8080);
console.log('server listening on port 8080');
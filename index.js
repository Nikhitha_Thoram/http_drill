const http = require("http");

const server = http.createServer((request, response) => {
  if (request.method === "GET" && request.url==='/html') {
    response.writeHead(200, { "content-type": "text/html" });
    response.write(
      ` <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>

        </body>
        </html>`
    )
    response.end();
  }
  else{
    response.writeHead(400,{'content-type':'text/html'})
    response.end('404 not found');
  }
});

server.listen(8000);
console.log('server listening on port 8000')

const http = require('http');

const server = http.createServer((request,response)=>{
    if(request.method === 'GET' && request.url.startsWith('/delay/')){
        const delayInSeconds = request.url.split('/')[2];
        if(!isNaN(delayInSeconds)){
            setTimeout(()=>{
                const statusCode=http.STATUS_CODES[200];
                response.write(`response after ${delayInSeconds}seconds with status ${statusCode} `);
                response.end();
            },delayInSeconds*1000)
            
        }
    }
    else{
        response.writeHead(400);
        response.end();
    }
})

server.listen(8080);
console.log('server listening on port 8080');
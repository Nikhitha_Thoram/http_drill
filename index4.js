
const http = require('http');

const server = http.createServer((request, response) => {
    const statusCode = Number(request.url.split('/')[2]);

    if (request.method === 'GET' && request.url.startsWith('/status/') && !isNaN(statusCode)) {
        response.writeHead(statusCode);
        response.end(`Status code with response ${statusCode}`);
    } else {
        response.writeHead(400, { 'content-type': 'text/plain' });
        response.end('Not found');
    }
});

server.listen(8080);
console.log('Server listening on port 8080');

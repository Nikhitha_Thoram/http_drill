
const http = require('http');

const server = http.createServer((request,response)=>{
    if(request.method==='GET' && request.url==='/json'){
        response.writeHead(200,{'content-type':'application/json'});
        response.write(`
            {
                "slideshow": {
                  "author": "Yours Truly",
                  "date": "date of publication",
                  "slides": [
                    {
                      "title": "Wake up to WonderWidgets!",
                      "type": "all"
                    },
                    {
                      "items": [
                        "Why <em>WonderWidgets</em> are great",
                        "Who <em>buys</em> WonderWidgets"
                      ],
                      "title": "Overview",
                      "type": "all"
                    }
                  ],
                  "title": "Sample Slide Show"
                }
              }
        `)
        response.end();
    }
    else{
        response.writeHead(400,{'content-type':'application/json'});
        response.end('404 not found');
    }
})

server.listen(8080);
console.log('server listening on port 8080')